﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WSCorso.Models
{
    public class ResponseDTO
    {
        public double result { get; set; }
        public string errorDescription { get; set; }
        public bool inError { get; set; }
    }
}