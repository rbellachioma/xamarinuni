﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WSCorso.Models
{
    public class ShoppingItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
    }
}