﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WSCorso.Models
{
    public class DbSingleton
    {
        public static DbSingleton instance;
        public List<ShoppingItem> listItems;

        private DbSingleton()
        {
            listItems = new List<ShoppingItem> {
            new ShoppingItem {Id=1, Name="Acqua", Quantity=2 },
            new ShoppingItem {Id=2, Name="Pane", Quantity=1 },
            new ShoppingItem {Id=3, Name="Pasta", Quantity=4 },
            new ShoppingItem {Id=4, Name="Biscotti", Quantity=5 },
            new ShoppingItem {Id=5, Name="Detersivo", Quantity=3 },
            new ShoppingItem {Id=6, Name="Piatti", Quantity=100 },
            new ShoppingItem {Id=7, Name="Bicchieri", Quantity=200 },
            new ShoppingItem {Id=8, Name="Posate", Quantity=50 },
            new ShoppingItem {Id=9, Name="Pentole", Quantity=2 },
            new ShoppingItem {Id=10, Name="Coperchi", Quantity=2 },
            new ShoppingItem {Id=11, Name="Coca-Cola", Quantity=5 },
            new ShoppingItem {Id=12, Name="Birre", Quantity=24 },
            new ShoppingItem {Id=13, Name="Patatine", Quantity=32 },
            new ShoppingItem {Id=14, Name="Pistacchi", Quantity=4 },
            new ShoppingItem {Id=15, Name="Passata", Quantity=3},
            new ShoppingItem {Id=16, Name="Merendine", Quantity=6 },
            new ShoppingItem {Id=17, Name="Crostatine", Quantity=8 },
            new ShoppingItem {Id=18, Name="Pizze", Quantity=43},
            new ShoppingItem {Id=19, Name="Spaghetti", Quantity=45 },
            new ShoppingItem {Id=20, Name="Pile", Quantity=26 }
        };
        }

        public static DbSingleton getInstance()
        {
            if (instance == null)
            {
                instance = new DbSingleton();
            }
            return instance;

        }
    }
}