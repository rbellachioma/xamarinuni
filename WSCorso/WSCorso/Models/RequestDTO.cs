﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WSCorso.Models
{
    public class RequestDTO
    {
        public double numToCalculate { get; set; }
        public double pow { get; set; }
        public string username { get; set; }
    }
}