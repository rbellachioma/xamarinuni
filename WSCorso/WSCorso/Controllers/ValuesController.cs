﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using WSCorso.Models;

namespace WSCorso.Controllers
{
    public class ValuesController : ApiController
    {

        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        public IEnumerable<ShoppingItem> GetShoppingItems()
        {
            return DbSingleton.getInstance().listItems.OrderBy(c => c.Name);
        }

        [HttpPost]
        public bool PostDeleteShoppingItem(ShoppingItem item)
        {
            try
            {
                var itemToRemove = DbSingleton.getInstance().listItems.Where(c => c.Id == item.Id).FirstOrDefault();
                DbSingleton.getInstance().listItems.Remove(itemToRemove);
                return true;
            }
            catch (Exception ex) { return false; }
        }

        [HttpPost]
        public ShoppingItem PostIncrementShoppingItem(ShoppingItem item)
        {
            try
            {
                foreach (var elem in DbSingleton.getInstance().listItems.Where(c => c.Id == item.Id))
                {
                    elem.Quantity = elem.Quantity + 1;
                    return elem;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpPost]
        public bool AddNewShoppingItem(ShoppingItem item)
        {
            try
            {
                int maxIndex = DbSingleton.getInstance().listItems.Max(c => c.Id);
                item.Id = maxIndex + 1;
                item.Quantity = 0;
                DbSingleton.getInstance().listItems.Add(item);
                return true;
            }
            catch (Exception ex) { return false; }
        }

        public double GetPow(double id)
        {
            return Math.Pow(id, 2);
        }

        public double GetPow2(double numtc, double pow)
        {
            return Math.Pow(numtc, pow);
        }

        [HttpPost]
        public ResponseDTO GetPowPost(RequestDTO id)
        {
            ResponseDTO resp = new ResponseDTO();
            if (id.username.Equals("pippo"))
            {
                resp.inError = false;
                resp.result = Math.Pow(id.numToCalculate, id.pow);
            }
            else
            {
                resp.inError = true;
                resp.errorDescription = "Non autorizzato";
            }
            return resp;
        }
    }
}
