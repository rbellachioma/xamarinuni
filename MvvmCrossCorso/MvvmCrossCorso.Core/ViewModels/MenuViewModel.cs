﻿using MvvmCross.Core.ViewModels;
using System.Windows.Input;

namespace MvvmCrossCorso.Core.ViewModels
{
    public class MenuViewModel : MvxViewModel
    {
        public ICommand NavigateToFirst { get; set; }
        public ICommand NavigateToCalculator { get; set; }
        public ICommand NavigateToShoppingList { get; set; }
        public ICommand NavigateToEventList { get; set; }

        public MenuViewModel()
        {
            NavigateToFirst = new MvxCommand(() => ShowViewModel<FirstViewModel>());
            NavigateToCalculator = new MvxCommand(() => ShowViewModel<CalculatorViewModel>());
            NavigateToShoppingList = new MvxCommand(() => ShowViewModel<ShoppingListViewModel>());
            NavigateToEventList = new MvxCommand(() => ShowViewModel<EventListViewModel>());
        }
    }
}
