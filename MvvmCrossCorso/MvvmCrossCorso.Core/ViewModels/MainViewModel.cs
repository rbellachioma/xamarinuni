﻿using MvvmCross.Core.ViewModels;

namespace MvvmCrossCorso.Core.ViewModels
{
    public class MainViewModel : MvxViewModel
    {
        public void ShowMenu()
        {
            ShowViewModel<CalculatorViewModel>();
            ShowViewModel<MenuViewModel>();
        }
    }
}
