﻿using MvvmCross.Core.Navigation;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;
using MvvmCrossCorso.Core.Contracts;
using MvvmCrossCorso.Core.Messages;
using MvvmCrossCorso.Core.Model;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace MvvmCrossCorso.Core.ViewModels
{
    public class EventListViewModel : MvxViewModel
    {
        public ObservableCollection<EventItemViewModel> EventItems { get; }
        readonly MvxSubscriptionToken token;
        readonly IEventService eventService;
        readonly IMvxNavigationService navigationService;
        readonly IMvxMessenger messenger;

        public IMvxAsyncCommand ShowAddNewEventItemCommand { get; }
        public IMvxAsyncCommand<EventItemViewModel> EventItemClickedCommand { get; }

        public EventListViewModel(IEventService eventService, IMvxNavigationService navigationService, IMvxMessenger messenger)
        {
            this.eventService = eventService;
            this.navigationService = navigationService;
            this.messenger = messenger;

            EventItems = new ObservableCollection<EventItemViewModel>();
            ShowAddNewEventItemCommand = new MvxAsyncCommand(ShowNewItem);
            EventItemClickedCommand = new MvxAsyncCommand<EventItemViewModel>(EditEventItem);

            token = messenger.SubscribeOnMainThread<EventItemChangedMessage>(async m => await LoadEventItems());
        }

        public async override void Start()
        {
            base.Start();
            await LoadEventItems();
        }

        public async Task LoadEventItems()
        {
            try
            {
                EventItems.Clear();
                var items = await eventService.GetAllEvents();
                foreach (var item in items)
                {
                    var viewModel = new EventItemViewModel(eventService, navigationService, messenger);
                    await viewModel.Initialize(item);
                    EventItems.Add(viewModel);
                }
            }
            catch (Exception)
            {

            }
        }

        async Task ShowNewItem()
        {
            EventItem item = new EventItem();
            item.EventDate = DateTime.Now;
            await navigationService.Navigate<EventItemViewModel, EventItem>(item);
        }

        async Task EditEventItem(EventItemViewModel itemClicked)
        {
            await navigationService.Navigate(itemClicked);
        }
    }
}
