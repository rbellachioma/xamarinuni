﻿using MvvmCross.Core.ViewModels;
using MvvmCrossCorso.Core.Contracts;
using MvvmCrossCorso.Core.Model;
using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MvvmCrossCorso.Core.ViewModels
{
    public class CalculatorViewModel : MvxViewModel
    {
        double number;

        public string Number
        {
            get
            {
                return Convert.ToString(number);
            }
            set
            {
                SetProperty(ref number, Convert.ToDouble(value));
            }
        }

        string result;

        public string Result
        {
            get
            {
                return result;
            }
            set
            {
                SetProperty(ref result, value);
            }
        }

        string resultPow;

        public string ResultPow
        {
            get
            {
                return resultPow;
            }
            set
            {
                SetProperty(ref resultPow, value);
            }
        }

        bool isBusy = false;

        public bool IsBusy
        {
            get
            {
                return isBusy;
            }
            set { SetProperty(ref isBusy, value); }
        }

        bool isBusyPow = false;

        public bool IsBusyPow
        {
            get
            {
                return isBusyPow;
            }
            set { SetProperty(ref isBusyPow, value); }
        }

        readonly ISquareRootCalculator calc;
        readonly IWebClientService cli;

        public ICommand SqrtCommand { get; set; }
        public ICommand SqrtAsyncCommand { get; set; }
        public ICommand PowAsyncCommand { get; set; }

        public CalculatorViewModel(ISquareRootCalculator calculator, IWebClientService client)
        {
            calc = calculator;
            cli = client;
            SqrtCommand = new MvxCommand(() => executeSqrtCalc(), CanStartNewCalc);
            SqrtAsyncCommand = new MvxAsyncCommand(() => executeSqrtAsyncCalc(), CanStartNewCalc);
            PowAsyncCommand = new MvxAsyncCommand(() => executePowAsyncCalc(), CanStartNewCalcPow);
        }

        public async Task executeSqrtAsyncCalc()
        {
            try
            {
                IsBusy = true;
                Result = await Task.Run(() => calc.Calculate(number).ToString());
                IsBusy = false;
            }
            catch (Exception)
            {
                IsBusy = false;
            }
        }

        public async Task executePowAsyncCalc()
        {
            try
            {
                IsBusyPow = true;
                RequestDTO req = new RequestDTO();
                req.numToCalculate = number;
                req.pow = 2;
                req.username = "pippo";
                var t = await Task.Run(() => cli.postPow(req));
                ResultPow = t.result.ToString();
                IsBusyPow = false;
            }
            catch (Exception)
            {
                IsBusyPow = false;
            }
        }

        public void executeSqrtCalc()
        {
            Result = calc.Calculate(number).ToString();
        }

        public bool CanStartNewCalc()
        {
            return !IsBusy;
        }

        public bool CanStartNewCalcPow()
        {
            return !IsBusyPow;
        }
    }
}
