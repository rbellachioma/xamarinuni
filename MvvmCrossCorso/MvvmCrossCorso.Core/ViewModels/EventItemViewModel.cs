﻿using MvvmCross.Core.Navigation;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;
using MvvmCrossCorso.Core.Contracts;
using MvvmCrossCorso.Core.Messages;
using MvvmCrossCorso.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvvmCrossCorso.Core.ViewModels
{
    public class EventItemViewModel : MvxViewModel<EventItem>
    {
        EventItem item;

        public string Description
        {
            get
            {
                return item.Description;
            }
            set
            {
                if (Description == value)
                {
                    return;
                }
                item.Description = value;
                RaisePropertyChanged();
            }
        }

        public DateTime EventDate
        {
            get
            {
                return item.EventDate;
            }
            set
            {
                if (EventDate == value)
                {
                    return;
                }
                item.EventDate = value;
                RaisePropertyChanged();
            }
        }

        public int? Guests
        {
            get
            {
                return item.Guests;
            }
            set
            {
                if (Guests == value)
                {
                    return;
                }
                item.Guests = value;
                RaisePropertyChanged();
            }
        }

        public int? Type
        {
            get
            {
                return item.Type;
            }
            set
            {
                if (Type == value)
                {
                    return;
                }
                item.Type = value;
                RaisePropertyChanged();
            }
        }

        private List<ComboItem> eventTypes;

        public List<ComboItem> EventTypes
        {
            get
            {
                if (eventTypes != null)
                {
                    return eventTypes;
                }
                eventTypes = new List<ComboItem>();
                eventTypes.Add(new ComboItem("<Selezionare un tipo>", null));
                eventTypes.Add(new ComboItem("Pranzo", 1));
                eventTypes.Add(new ComboItem("Cena", 2));
                eventTypes.Add(new ComboItem("Aperitivo", 3));

                return eventTypes;
            }
        }

        private ComboItem selectedType;

        public ComboItem SelectedType
        {
            get
            {
                if (selectedType == null)
                {
                    selectedType = EventTypes.Where(c => c.ComboValue == null).FirstOrDefault();
                }
                return selectedType;
            }
            set
            {
                selectedType = value;
                item.Type = selectedType.ComboValue;
                RaisePropertyChanged(() => SelectedType);
            }

        }


        readonly IEventService eventService;
        readonly IMvxNavigationService navigationService;
        readonly IMvxMessenger messenger;

        public IMvxAsyncCommand DeleteCommand { get; }
        public IMvxAsyncCommand SaveCommand { get; }
        public IMvxAsyncCommand CancelCommand { get; }

        public EventItemViewModel(IEventService eventService, IMvxNavigationService navigationService, IMvxMessenger messenger)
        {
            this.eventService = eventService;
            this.navigationService = navigationService;
            this.messenger = messenger;

            DeleteCommand = new MvxAsyncCommand(Delete);
            SaveCommand = new MvxAsyncCommand(Save);
            CancelCommand = new MvxAsyncCommand(Cancel);
        }

        public override void Prepare(EventItem parameter)
        {
            base.Prepare();
            item = parameter;
        }

        public async Task Initialize(EventItem parameter)
        {
            await base.Initialize();
            item = parameter;
            selectedType = EventTypes.Where(c => c.ComboValue == item.Type).FirstOrDefault();
        }

        async Task Delete()
        {
            await eventService.DeleteEventItem(item);
            messenger.Publish(new EventItemChangedMessage(this));
        }

        async Task Save()
        {
            await eventService.InsertEventItem(item);
            await navigationService.Close(this);
            messenger.Publish(new EventItemChangedMessage(this));
        }

        async Task Cancel()
        {
            await navigationService.Close(this);
        }
    }
}
