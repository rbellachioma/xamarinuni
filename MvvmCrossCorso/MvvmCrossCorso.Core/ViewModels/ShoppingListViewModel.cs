﻿using MvvmCross.Core.Navigation;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;
using MvvmCrossCorso.Core.Contracts;
using MvvmCrossCorso.Core.Messages;
using MvvmCrossCorso.Core.Model;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace MvvmCrossCorso.Core.ViewModels
{
    public class ShoppingListViewModel : MvxViewModel
    {
        private bool hasError = false;

        public bool HasError
        {
            get { return hasError; }
            set { SetProperty(ref hasError, value); }
        }

        private string errorDesc = string.Empty;

        public string ErrorDesc
        {
            get { return errorDesc; }
            set { SetProperty(ref errorDesc, value); }
        }

        public ObservableCollection<ShoppingItemViewModel> ShoppingItems { get; }
        readonly MvxSubscriptionToken token;
        readonly IWebClientService webClientService;
        readonly IMvxNavigationService navigationService;
        readonly IMvxMessenger messenger;
        public IMvxAsyncCommand ShowAddNewShoppingItemCommand { get; }

        public ShoppingListViewModel(IWebClientService webClientService, IMvxNavigationService navigationService, IMvxMessenger messenger)
        {
            ShoppingItems = new ObservableCollection<ShoppingItemViewModel>();
            ShowAddNewShoppingItemCommand = new MvxAsyncCommand(ShowAddNewItem);
            this.webClientService = webClientService;
            this.navigationService = navigationService;
            this.messenger = messenger;
            token = messenger.SubscribeOnMainThread<ShoppingItemChangedMessage>(async m => await LoadShoppingItems());
        }

        public override async void Start()
        {
            base.Start();
            await LoadShoppingItems();
        }

        public async Task LoadShoppingItems()
        {
            try
            {
                HasError = false;
                ErrorDesc = string.Empty;

                ShoppingItems.Clear();
                var newItems = await webClientService.getAllShoppingItems();
                foreach (var item in newItems)
                {
                    var viewModel = new ShoppingItemViewModel(webClientService, navigationService, messenger);
                    await viewModel.Initialize(item);
                    ShoppingItems.Add(viewModel);
                }
            }
            catch (Exception)
            {
                ErrorDesc = "Non riesco a contattare il ws";
                HasError = true;
            }
        }

        async Task ShowAddNewItem()
        {
            await navigationService.Navigate<ShoppingItemViewModel, ShoppingItem>(new ShoppingItem());
        }
    }
}
