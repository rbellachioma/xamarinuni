﻿using MvvmCross.Core.Navigation;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;
using MvvmCrossCorso.Core.Contracts;
using MvvmCrossCorso.Core.Messages;
using MvvmCrossCorso.Core.Model;
using System.Threading.Tasks;

namespace MvvmCrossCorso.Core.ViewModels
{
    public class ShoppingItemViewModel : MvxViewModel<ShoppingItem>
    {
        ShoppingItem item;

        public string Name
        {
            get { return item.Name; }
            set
            {
                if (Name == value)
                {
                    return;
                }
                item.Name = value;
                RaisePropertyChanged();
            }
        }

        public int Quantity
        {
            get
            {
                return item.Quantity;
            }
        }

        readonly IWebClientService webClientService;
        readonly IMvxNavigationService navigationService;
        readonly IMvxMessenger messenger;
        public IMvxAsyncCommand IncrementCommand { get; }
        public IMvxAsyncCommand DeleteCommand { get; }
        public IMvxAsyncCommand CancelCommand { get; }
        public IMvxAsyncCommand SaveCommand { get; }

        public ShoppingItemViewModel(IWebClientService webClientService, IMvxNavigationService navigationService, IMvxMessenger messenger)
        {
            this.webClientService = webClientService;
            this.navigationService = navigationService;
            this.messenger = messenger;
            IncrementCommand = new MvxAsyncCommand(IncrementQuantity);
            DeleteCommand = new MvxAsyncCommand(Delete);
            CancelCommand = new MvxAsyncCommand(Cancel);
            SaveCommand = new MvxAsyncCommand(Save);
        }

        public override void Prepare(ShoppingItem parameter)
        {
            base.Prepare();
            item = parameter;
        }

        public async Task Initialize(ShoppingItem parameter)
        {
            await base.Initialize();
            item = parameter;
        }

        async Task IncrementQuantity()
        {
            var response = await webClientService.incrementShoppingItem(item);
            item.Quantity = response.Quantity;
            RaisePropertyChanged(() => Quantity);
        }

        async Task Delete()
        {
            await webClientService.deleteShoppingItem(item);
            messenger.Publish(new ShoppingItemChangedMessage(this));
        }

        async Task Cancel()
        {
            await navigationService.Close(this);
        }

        async Task Save()
        {
            await webClientService.addNewShoppingItem(item);
            await navigationService.Close(this);
            messenger.Publish(new ShoppingItemChangedMessage(this));
        }
    }
}
