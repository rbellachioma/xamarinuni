﻿using MvvmCrossCorso.Core.Contracts;
using MvvmCrossCorso.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvvmCrossCorso.Core.Services
{
    public class EventService : IEventService
    {
        readonly IEventRepository repository;
        public EventService(IEventRepository repository)
        {
            this.repository = repository;
        }

        public async Task DeleteEventItem(EventItem eventItem)
        {
            await repository.Delete(eventItem);
        }

        public Task<List<EventItem>> GetAllEvents()
        {
            return repository.GetAll();
        }

        public async Task InsertEventItem(EventItem eventItem)
        {
            eventItem.EventDateHR = eventItem.EventDate.ToString("yyyyMMdd");
            await repository.Save(eventItem);
        }
    }
}
