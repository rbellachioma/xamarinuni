﻿using MvvmCrossCorso.Core.Contracts;
using System;
using System.Threading.Tasks;

namespace MvvmCrossCorso.Core.Services
{
    public class SquareRootCalculator : ISquareRootCalculator
    {
        public double Calculate(double numberToCalculate)
        {
            Task.Delay(TimeSpan.FromSeconds(10)).Wait();
            return Math.Sqrt(numberToCalculate);
        }
    }
}
