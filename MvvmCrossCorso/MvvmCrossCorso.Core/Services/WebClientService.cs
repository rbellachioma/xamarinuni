﻿using MvvmCrossCorso.Core.Contracts;
using MvvmCrossCorso.Core.Model;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace MvvmCrossCorso.Core.Services
{
    public class WebClientService : IWebClientService
    {
        readonly HttpClient httpClient = new HttpClient();
        private string baseUrl = "http://c1e623f9.ngrok.io/";

        public async Task<double> getPow(double num, double pow)
        {
            var url = baseUrl + string.Format("api/values/GetPow2?numtc={0}&pow={1}", num, pow);
            var response = await httpClient.GetAsync(url).ConfigureAwait(false);
            var json = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            return JsonConvert.DeserializeObject<double>(json);
        }

        public async Task<ResponseDTO> postPow(RequestDTO req)
        {
            var url = baseUrl + "api/values/GetPowPost";
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = await httpClient.PostAsync(url, new StringContent(JsonConvert.SerializeObject(req), Encoding.UTF8, "application/json"));
            var json = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            return JsonConvert.DeserializeObject<ResponseDTO>(json);
        }

        public async Task<List<ShoppingItem>> getAllShoppingItems()
        {
            var url = baseUrl + string.Format("api/values/GetShoppingItems");
            var response = await httpClient.GetAsync(url).ConfigureAwait(false);
            var json = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            return JsonConvert.DeserializeObject<List<ShoppingItem>>(json);
        }

        public async Task<ShoppingItem> incrementShoppingItem(ShoppingItem shoppingItem)
        {
            var url = baseUrl + string.Format("api/values/PostIncrementShoppingItem");
            var response = await httpClient.PostAsync(url, new StringContent(JsonConvert.SerializeObject(shoppingItem), Encoding.UTF8, "application/json"));
            var json = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            return JsonConvert.DeserializeObject<ShoppingItem>(json);
        }

        public async Task<bool> deleteShoppingItem(ShoppingItem shoppingItem)
        {
            var url = baseUrl + string.Format("api/values/PostDeleteShoppingItem");
            var response = await httpClient.PostAsync(url, new StringContent(JsonConvert.SerializeObject(shoppingItem), Encoding.UTF8, "application/json"));
            var json = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            return JsonConvert.DeserializeObject<bool>(json);
        }
        public async Task<bool> addNewShoppingItem(ShoppingItem shoppingItem)
        {
            var url = baseUrl + string.Format("api/values/AddNewShoppingItem");
            var response = await httpClient.PostAsync(url, new StringContent(JsonConvert.SerializeObject(shoppingItem), Encoding.UTF8, "application/json"));
            var json = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            return JsonConvert.DeserializeObject<bool>(json);
        }
    }
}
