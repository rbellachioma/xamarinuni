﻿using MvvmCrossCorso.Core.Contracts;
using MvvmCrossCorso.Core.Model;
using PCLStorage;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvvmCrossCorso.Core.Repository
{
    public class EventRepository : IEventRepository
    {
        SQLiteAsyncConnection connection;
        public EventRepository()
        {
            var local = FileSystem.Current.LocalStorage.Path;
            var dataFile = Path.Combine(local, "unihouse.db3");
            connection = new SQLiteAsyncConnection(dataFile);
            connection.GetConnection().CreateTable<EventItem>();
        }

        public Task Delete(EventItem eventItem)
        {
            return connection.DeleteAsync(eventItem);
        }

        public Task<List<EventItem>> GetAll()
        {
            return connection.Table<EventItem>().OrderByDescending(e => e.EventDate).ToListAsync();
        }

        public Task Save(EventItem eventItem)
        {
            return connection.InsertOrReplaceAsync(eventItem);
        }
    }
}
