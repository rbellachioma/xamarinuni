﻿using MvvmCrossCorso.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvvmCrossCorso.Core.Contracts
{
    public interface IEventService
    {
        Task InsertEventItem(EventItem eventItem);
        Task DeleteEventItem(EventItem eventItem);
        Task<List<EventItem>> GetAllEvents();
    }
}
