﻿namespace MvvmCrossCorso.Core.Contracts
{
    public interface ISquareRootCalculator
    {
        double Calculate(double numberToCalculate);
    }
}
