﻿using MvvmCrossCorso.Core.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MvvmCrossCorso.Core.Contracts
{
    public interface IWebClientService
    {

        Task<double> getPow(double num, double pow);

        Task<ResponseDTO> postPow(RequestDTO req);

        Task<ShoppingItem> incrementShoppingItem(ShoppingItem shoppingItem);

        Task<List<ShoppingItem>> getAllShoppingItems();

        Task<bool> addNewShoppingItem(ShoppingItem shoppingItem);

        Task<bool> deleteShoppingItem(ShoppingItem shoppingItem);
    }
}
