﻿using MvvmCrossCorso.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvvmCrossCorso.Core.Contracts
{
    public interface IEventRepository
    {
        Task Save(EventItem eventItem);
        Task<List<EventItem>> GetAll();
        Task Delete(EventItem eventItem);
    }
}
