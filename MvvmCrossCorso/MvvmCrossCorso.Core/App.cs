using MvvmCross.Platform;
using MvvmCross.Platform.IoC;
using MvvmCrossCorso.Core.Contracts;
using MvvmCrossCorso.Core.Services;

namespace MvvmCrossCorso.Core
{
    public class App : MvvmCross.Core.ViewModels.MvxApplication
    {
        public override void Initialize()
        {
            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();

            CreatableTypes()
                .EndingWith("Repository")
                .AsInterfaces()
                .RegisterAsLazySingleton();

            Mvx.LazyConstructAndRegisterSingleton<ISquareRootCalculator, SquareRootCalculator>();
            RegisterNavigationServiceAppStart<ViewModels.MainViewModel>();
            //RegisterAppStart<ViewModels.ShoppingListViewModel>();
        }
    }
}
