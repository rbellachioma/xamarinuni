﻿using MvvmCross.Platform.Converters;
using System;
using System.Globalization;

namespace MvvmCrossCorso.Core.Converters
{
    public class DateStringValueConverter : MvxValueConverter<DateTime, string>
    {
        protected override string Convert(DateTime value, Type targetType, object parameter, CultureInfo cultureInfo)
        {
            return value.ToString("dd/MM/yyyy");
        }
    }
}
