﻿using MvvmCross.Plugins.Messenger;

namespace MvvmCrossCorso.Core.Messages
{
    public class EventItemChangedMessage : MvxMessage
    {
        public EventItemChangedMessage(object sender) : base(sender)
        {
        }
    }
}
