﻿using MvvmCross.Plugins.Messenger;

namespace MvvmCrossCorso.Core.Messages
{
    public class ShoppingItemChangedMessage : MvxMessage
    {
        public ShoppingItemChangedMessage(object sender) : base(sender)
        {
        }
    }
}
