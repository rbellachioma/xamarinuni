﻿namespace MvvmCrossCorso.Core.Model
{
    public class ComboItem
    {
        public ComboItem(string text, int? value)
        {
            ComboText = text;
            ComboValue = value;
        }

        public int? ComboValue { get; private set; }

        public string ComboText { get; private set; }

        public override string ToString()
        {
            if (ComboValue == null)
            {
                return string.Empty;
            }
            return ComboText;
        }
    }
}
