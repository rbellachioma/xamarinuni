﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvvmCrossCorso.Core.Model
{
    public class EventItem
    {
        [PrimaryKey]
        [AutoIncrement]
        public int? Id { get; set; }
        public DateTime EventDate { get; set; }
        public string EventDateHR { get; set; }
        public string Description { get; set; }
        public int? Guests { get; set; }
        public int? Type { get; set; }
    }
}
