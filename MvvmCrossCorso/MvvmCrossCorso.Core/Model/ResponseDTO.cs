﻿namespace MvvmCrossCorso.Core.Model
{
    public class ResponseDTO
    {
        public double result { get; set; }
        public string errorDescription { get; set; }
        public bool inError { get; set; }
    }
}
