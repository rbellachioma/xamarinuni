﻿namespace MvvmCrossCorso.Core.Model
{
    public class RequestDTO
    {
        public double numToCalculate { get; set; }
        public double pow { get; set; }
        public string username { get; set; }
    }
}
