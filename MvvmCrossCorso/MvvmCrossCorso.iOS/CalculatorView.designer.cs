// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace MvvmCrossCorso.iOS
{
    [Register ("CalculatorView")]
    partial class CalculatorView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnPowAsync { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnSqrt { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnSqrtAsync { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblPowResult { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblResultSqrt { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblSqrt { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField txtNumber { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (btnPowAsync != null) {
                btnPowAsync.Dispose ();
                btnPowAsync = null;
            }

            if (btnSqrt != null) {
                btnSqrt.Dispose ();
                btnSqrt = null;
            }

            if (btnSqrtAsync != null) {
                btnSqrtAsync.Dispose ();
                btnSqrtAsync = null;
            }

            if (lblPowResult != null) {
                lblPowResult.Dispose ();
                lblPowResult = null;
            }

            if (lblResultSqrt != null) {
                lblResultSqrt.Dispose ();
                lblResultSqrt = null;
            }

            if (lblSqrt != null) {
                lblSqrt.Dispose ();
                lblSqrt = null;
            }

            if (txtNumber != null) {
                txtNumber.Dispose ();
                txtNumber = null;
            }
        }
    }
}