﻿using Foundation;
using MvvmCross.Core.ViewModels;
using MvvmCross.iOS.Platform;
using MvvmCross.Platform;
using UIKit;

namespace MvvmCrossCorso.iOS
{
    [Register("AppDelegate")]
    public partial class AppDelegate : MvxApplicationDelegate
    {
        public override UIWindow Window
        {
            get;
            set;
        }

        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            //UINavigationBar.Appearance.BarTintColor = new UIColor(red: 0.19f, green: 0.25f, blue: 0.62f, alpha: 1.0f);
            //UINavigationBar.Appearance.TintColor = UIColor.DarkGray;
            //UINavigationBar.Appearance.TitleTextAttributes = new UIStringAttributes(
                //new NSDictionary(UIStringAttributeKey.ForegroundColor, UIColor.DarkGray));

            Window = new UIWindow(UIScreen.MainScreen.Bounds);

            var setup = new Setup(this, Window);
            setup.Initialize();

            var startup = Mvx.Resolve<IMvxAppStart>();
            startup.Start();

            Window.MakeKeyAndVisible();

            return true;
        }
    }
}
