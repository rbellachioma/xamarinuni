﻿using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views;
using MvvmCrossCorso.Core.ViewModels;
using System;

namespace MvvmCrossCorso.iOS.Views
{
    public partial class ShoppingItemTableViewCell : MvxTableViewCell
    {
        public ShoppingItemTableViewCell(IntPtr handle) : base(handle)
        {
            this.DelayBind(() =>
            {
                var set = this.CreateBindingSet<ShoppingItemTableViewCell, ShoppingItemViewModel>();
                set.Bind(lblName).To(vm => vm.Name);
                set.Bind(lblQuantity).To(vm => vm.Quantity);
                set.Bind(btnAdd).To(vm => vm.IncrementCommand);
                set.Apply();
            }
            );
        }
    }

}
