﻿using MvvmCross.Binding.BindingContext;
using MvvmCross.iOS.Views;
using MvvmCross.Platform.WeakSubscription;
using MvvmCrossCorso.Core.ViewModels;
using System;
using UIKit;

namespace MvvmCrossCorso.iOS.Views
{
    [MvxFromStoryboard]
    public partial class ShoppingListView : MvxTableViewController<ShoppingListViewModel>
    {
        MvxNamedNotifyPropertyChangedEventSubscription<bool> token;
        UIRefreshControl refreshControl;
        public ShoppingListView(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            var source = new ShoppingListViewSource(TableView);
            TableView.Source = source;

            var btnNewItem = new UIBarButtonItem(UIBarButtonSystemItem.Add);
            NavigationItem.SetRightBarButtonItem(btnNewItem, false);

            refreshControl = new UIRefreshControl();
            refreshControl.ValueChanged += RefreshControl_valueChanged;

            TableView.AddSubview(refreshControl);

            var set = this.CreateBindingSet<ShoppingListView, ShoppingListViewModel>();
            set.Bind(source).To(vm => vm.ShoppingItems);
            set.Bind(btnNewItem).To(vm => vm.ShowAddNewShoppingItemCommand);
            set.Apply();


            token = ViewModel.WeakSubscribe(() =>
                                            ViewModel.HasError, (sender, e) =>
                                            {
                                                if (ViewModel.HasError)
                                                {
                                                    ShowAlert(ViewModel.ErrorDesc);
                                                }
                                            });
        }

        async void RefreshControl_valueChanged(object sender, EventArgs e)
        {
            refreshControl.BeginRefreshing();
            await ViewModel.LoadShoppingItems();
            refreshControl.EndRefreshing();
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
        }

        private void ShowAlert(string message)
        {
            var okalert = UIAlertController.Create("Attenzione", message, UIAlertControllerStyle.Alert);
            okalert.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));
            PresentViewController(okalert, false, null);
        }
    }
}

