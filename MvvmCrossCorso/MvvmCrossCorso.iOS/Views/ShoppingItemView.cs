using MvvmCross.Binding.BindingContext;
using MvvmCross.iOS.Views;
using MvvmCrossCorso.Core.ViewModels;
using System;
using UIKit;

namespace MvvmCrossCorso.iOS.Views
{
    [MvxFromStoryboard]
    public partial class ShoppingItemView : MvxViewController
    {
        public ShoppingItemView(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            var btnDone = new UIBarButtonItem(UIBarButtonSystemItem.Done);
            NavigationItem.SetRightBarButtonItem(btnDone, false);

            Title = "Nuovo";

            var set = this.CreateBindingSet<ShoppingItemView, ShoppingItemViewModel>();
            set.Bind(lblNewItemName).To(vm => vm.Name);
            set.Bind(btnDone).To(vm=>vm.SaveCommand);
            set.Apply();
        }
    }
}