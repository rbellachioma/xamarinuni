﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace MvvmCrossCorso.iOS.Views
{
    [Register ("ShoppingItemView")]
    partial class ShoppingItemView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField lblNewItemName { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (lblNewItemName != null) {
                lblNewItemName.Dispose ();
                lblNewItemName = null;
            }
        }
    }
}