﻿using MvvmCross.Core.ViewModels;
using MvvmCross.iOS.Views;
using MvvmCross.iOS.Views.Presenters.Attributes;
using MvvmCross.Platform;
using MvvmCrossCorso.Core.ViewModels;
using UIKit;

namespace MvvmCrossCorso.iOS.Views
{
    [MvxRootPresentation(WrapInNavigationController = false)]
    public sealed partial class TabBarController : MvxTabBarViewController<MainViewModel>
    {
        private bool constructed;

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            if (!constructed)
            {
                return;
            }

            CalculatorViewModel calcViewModel = (CalculatorViewModel)Mvx.IocConstruct(typeof(CalculatorViewModel));
            FirstViewModel firstViewModel = (FirstViewModel)Mvx.IocConstruct(typeof(FirstViewModel));

            var mvxViewModelLoader = Mvx.Resolve<IMvxViewModelLoader>();
            var vmRequest = MvxViewModelRequest.GetDefaultRequest(typeof(ShoppingListViewModel));
            ShoppingListViewModel shoppingListViewModel = mvxViewModelLoader.LoadViewModel(vmRequest, null) as ShoppingListViewModel;

            var viewControllers = new[] {
                CreateTabFor(0,"Calcolatrice", "Calc", calcViewModel ),
                CreateTabFor(1,"First", "First", firstViewModel ),
                CreateTabFor(2,"Lista spesa", "ShopList", shoppingListViewModel ),
                CreateTabFor(3,"Eventi", "EventList", shoppingListViewModel )
            };

            ViewControllers = viewControllers;
            CustomizableViewControllers = new UIViewController[] { };
            SelectedViewController = ViewControllers[0];
        }

        private UIViewController CreateTabFor(int index, string title, string imageName, IMvxViewModel viewModel)
        {
            var controller = new UINavigationController();
            controller.NavigationBar.TintColor = UIColor.Black;

            var screen = this.CreateViewControllerFor(viewModel) as UIViewController;
            screen.Title = title;
            screen.TabBarItem = new UITabBarItem(title, UIImage.FromBundle(imageName), index);
            controller.PushViewController(screen, false);

            return controller;
        }

        public TabBarController()
        {
            constructed = true;
            ViewDidLoad();
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
    }
}

