﻿using MvvmCross.Binding.BindingContext;
using MvvmCross.iOS.Views;
using MvvmCrossCorso.Core.ViewModels;
using System;
using UIKit;


namespace MvvmCrossCorso.iOS.Views
{
    [MvxFromStoryboard]
    public partial class CalculatorView : MvxViewController
    {
        public CalculatorView(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            DismissKeyboardOnBackgroundTap();

            var set = this.CreateBindingSet<CalculatorView, CalculatorViewModel>();
            set.Bind(lblResultSqrt).To(vm => vm.Result);
            set.Bind(txtNumber).To(vm => vm.Number);
            set.Bind(lblPowResult).To(vm=>vm.ResultPow);
            set.Bind(btnSqrt).To(vm=>vm.SqrtCommand);
            set.Bind(btnSqrtAsync).To(vm => vm.SqrtAsyncCommand);
            set.Bind(btnPowAsync).To(vm => vm.PowAsyncCommand);
            set.Bind(loadSqrt).For("Hidden").To(vm => vm.IsBusy).WithConversion("Visibility");
            set.Bind(lblLoadingSqrt).For("Hidden").To(vm => vm.IsBusy).WithConversion("Visibility");
            set.Bind(lblResultSqrt).For("Hidden").To(vm => vm.IsBusy).WithConversion("InvertedVisibility");
            set.Apply();

            this.txtNumber.ResignFirstResponder();
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
        }

        protected void DismissKeyboardOnBackgroundTap()
        {
            var tap = new UITapGestureRecognizer { CancelsTouchesInView = false };
            tap.AddTarget(() => View.EndEditing(true));
            View.AddGestureRecognizer(tap);
        }
    }
}

