﻿using Foundation;
using MvvmCross.Binding.iOS.Views;
using MvvmCrossCorso.Core.ViewModels;
using UIKit;

namespace MvvmCrossCorso.iOS.Views
{
    public class ShoppingListViewSource : MvxTableViewSource
    {
        public ShoppingListViewSource(UITableView tableView) : base(tableView)
        {
        }

        protected override UITableViewCell GetOrCreateCellFor(UITableView tableView, NSIndexPath indexPath, object item)
        {
            return (ShoppingItemTableViewCell)TableView.DequeueReusableCell("ShoppingItemCell");
        }

        public override void CommitEditingStyle(UITableView tableView, UITableViewCellEditingStyle editingStyle, NSIndexPath indexPath)
        {
            var itemToDelete = (ShoppingItemViewModel)GetItemAt(indexPath);
            itemToDelete.DeleteCommand.Execute();
        }
    }
}
