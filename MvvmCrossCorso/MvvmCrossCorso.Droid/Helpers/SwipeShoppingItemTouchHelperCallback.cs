﻿using Android.Support.V7.Widget;
using Android.Support.V7.Widget.Helper;
using MvvmCrossCorso.Core.ViewModels;

namespace MvvmCrossCorso.Droid.Helpers
{
    public class SwipeShoppingItemTouchHelperCallback : ItemTouchHelper.SimpleCallback
    {
        readonly ShoppingListViewModel viewModel;
        public SwipeShoppingItemTouchHelperCallback(ShoppingListViewModel viewModel) : base(0, ItemTouchHelper.Start)
        {
            this.viewModel = viewModel;
        }

        public override bool OnMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target)
        {
            return true;
        }
        public override void OnSwiped(RecyclerView.ViewHolder viewHolder,
        int direction)
        {
            viewModel.ShoppingItems[viewHolder.AdapterPosition].DeleteCommand.Execute();
        }
    }
}