﻿using Android.Content.Res;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Views;
using MvvmCross.Binding.Droid.BindingContext;
using MvvmCross.Core.ViewModels;
using MvvmCross.Droid.Support.V4;
using MvvmCross.Droid.Support.V7.AppCompat;
using MvvmCrossCorso.Droid.Activities;

namespace MvvmCrossCorso.Droid.Fragments
{
    public abstract class BaseFragment : MvxFragment
    {
        protected Toolbar toolbar { get; set; }
        protected MvxActionBarDrawerToggle drawerToggle { get; set; }
        protected bool showHamburgerMenu { get; set; } = false;
        protected string fragmentTitle { get; set; } = string.Empty;
        protected abstract int FragmentId { get; }

        protected BaseFragment()
        {
            RetainInstance = true;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);
            var view = this.BindingInflate(FragmentId, null);
            toolbar = view.FindViewById<Toolbar>(Resource.Id.toolbar);
            if (toolbar != null)
            {
                var mainActivity = Activity as MainActivity;
                if (mainActivity == null)
                {
                    return view;
                }
                mainActivity.SetSupportActionBar(toolbar);
                mainActivity.SupportActionBar.Title = fragmentTitle;

                if (showHamburgerMenu)
                {
                    mainActivity.SupportActionBar?.SetDisplayHomeAsUpEnabled(true);
                    mainActivity.SupportActionBar?.SetHomeButtonEnabled(true);
                    drawerToggle = new MvxActionBarDrawerToggle(
                        Activity,
                        mainActivity.drawerLayout,
                        toolbar,
                        Resource.String.drawer_open,
                        Resource.String.drawer_close
                        );
                    drawerToggle.DrawerOpened += (sender, eventArgs) => mainActivity?.HideSoftKeyBoard();
                    mainActivity.drawerLayout.AddDrawerListener(drawerToggle);
                }
            }
            return view;
        }

        public override void OnConfigurationChanged(Configuration newConfig)
        {
            base.OnConfigurationChanged(newConfig);
            if (toolbar != null)
            {
                drawerToggle?.OnConfigurationChanged(newConfig);
            }
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            base.OnActivityCreated(savedInstanceState);
            if (toolbar != null)
            {
                drawerToggle?.SyncState();
            }
        }
    }

    public abstract class BaseFragment<TViewModel> : BaseFragment where TViewModel : class, IMvxViewModel
    {
        public new TViewModel ViewModel
        {
            get { return (TViewModel)base.ViewModel; }
            set { base.ViewModel = value; }
        }
    }
}