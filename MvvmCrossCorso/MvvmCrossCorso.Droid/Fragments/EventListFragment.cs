﻿using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.Widget;
using Android.Support.V7.Widget;
using Android.Support.V7.Widget.Helper;
using Android.Views;
using MvvmCross.Droid.Support.V7.RecyclerView;
using MvvmCross.Droid.Views.Attributes;
using MvvmCross.Platform.WeakSubscription;
using MvvmCrossCorso.Core.ViewModels;
using MvvmCrossCorso.Droid.Helpers;
using System;


namespace MvvmCrossCorso.Droid.Fragments
{
    [MvxFragmentPresentation(typeof(MainViewModel), Resource.Id.content_frame)]
    [Register("com.companyname.MvvmCrossCorso.droid.fragments.EventListFragment")]
    public class EventListFragment : BaseFragment<EventListViewModel>
    {
        protected override int FragmentId => Resource.Layout.fragment_event_list;
        SwipeRefreshLayout refresher;
        View view;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            showHamburgerMenu = true;
            fragmentTitle = "Eventi";
            view = base.OnCreateView(inflater, container, savedInstanceState);
            var recyclerView = view.FindViewById<MvxRecyclerView>(Resource.Id.recycler_view);
            recyclerView.AddItemDecoration(new RecyclerViewItemDecoration(Activity));
            recyclerView.SetLayoutManager(new LinearLayoutManager(Activity));

            var callback = new SwipeEventItemTouchHelperCallback(ViewModel);
            var touchHelper = new ItemTouchHelper(callback);
            touchHelper.AttachToRecyclerView(recyclerView);

            refresher = view.FindViewById<SwipeRefreshLayout>(Resource.Id.refresher);
            refresher.Refresh += Refresher_Refresh;

            return view;
        }

        private async void Refresher_Refresh(object sender, EventArgs e)
        {
            await ViewModel.LoadEventItems();
            refresher.Refreshing = false;
        }
    }
}