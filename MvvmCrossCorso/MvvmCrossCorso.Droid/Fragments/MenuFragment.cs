﻿using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Views;
using MvvmCross.Binding.Droid.BindingContext;
using MvvmCross.Droid.Support.V4;
using MvvmCross.Droid.Views.Attributes;
using MvvmCrossCorso.Core.ViewModels;
using MvvmCrossCorso.Droid.Activities;
using System;
using System.Threading.Tasks;

namespace MvvmCrossCorso.Droid.Fragments
{
    [MvxFragmentPresentation(typeof(MainViewModel), Resource.Id.navigation_frame)]
    [Register("com.companyname.MvvmCrossCorso.droid.fragments.MenuFragment")]
    public class MenuFragment : MvxFragment<MenuViewModel>, NavigationView.IOnNavigationItemSelectedListener
    {
        private NavigationView navigationView;
        private IMenuItem previousMenuItem;
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);
            var view = this.BindingInflate(Resource.Layout.fragment_navigation, null);
            navigationView = view.FindViewById<NavigationView>(Resource.Id.navigation_view);
            navigationView.SetNavigationItemSelectedListener(this);
            // Imposto come primo elemento selezionato il view model calculator
            navigationView.Menu.FindItem(Resource.Id.nav_calculator).SetCheckable(true);
            navigationView.Menu.FindItem(Resource.Id.nav_calculator).SetChecked(true);
            previousMenuItem = navigationView.Menu.FindItem(Resource.Id.nav_calculator);
            return view;
        }

        public bool OnNavigationItemSelected(IMenuItem menuItem)
        {
            if (menuItem != previousMenuItem)
            {
                previousMenuItem?.SetChecked(false);
            }
            menuItem.SetCheckable(true);
            menuItem.SetChecked(true);

            previousMenuItem = menuItem;
            Navigate(menuItem.ItemId);
            return true;
        }

        private async Task Navigate(int itemId)
        {
            ((MainActivity)Activity).drawerLayout.CloseDrawers();

            await Task.Delay(TimeSpan.FromMilliseconds(50));

            switch (itemId)
            {
                case Resource.Id.nav_calculator:
                    ViewModel.NavigateToCalculator.Execute(null);
                    break;

                case Resource.Id.nav_first:
                    ViewModel.NavigateToFirst.Execute(null);
                    break;

                case Resource.Id.nav_shopping_list:
                    ViewModel.NavigateToShoppingList.Execute(null);
                    break;

                case Resource.Id.nav_event_list:
                    ViewModel.NavigateToEventList.Execute(null);
                    break;
            }
        }
    }
}