﻿using Android.OS;
using Android.Runtime;
using Android.Views;
using MvvmCross.Droid.Views.Attributes;
using MvvmCrossCorso.Core.ViewModels;

namespace MvvmCrossCorso.Droid.Fragments
{
    [MvxFragmentPresentation(typeof(MainViewModel), Resource.Id.content_frame)]
    [Register("com.companyname.MvvmCrossCorso.droid.fragments.CalculatorFragment")]
    public class CalculatorFragment : BaseFragment<CalculatorViewModel>
    {
        protected override int FragmentId => Resource.Layout.fragment_calculator;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            showHamburgerMenu = true;
            fragmentTitle = "Calcolatrice";
            return base.OnCreateView(inflater, container, savedInstanceState);
        }
    }
}