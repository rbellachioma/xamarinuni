﻿using Android.App;
using Android.OS;
using Android.Widget;
using MvvmCross.Droid.Support.V4;
using System;

namespace MvvmCrossCorso.Droid.Fragments
{
    public class DatePickerFragment : MvxDialogFragment, DatePickerDialog.IOnDateSetListener
    {
        // TAG can be any string of your choice.
        public static readonly string TAG = "X:" + typeof(DatePickerFragment).Name.ToUpper();
        private static DateTime _dateStart;

        // Initialize this value to prevent NullReferenceExceptions.
        Action<DateTime> _dateSelectedHandler = delegate { };

        public static DatePickerFragment NewInstance(DateTime dateStart, Action<DateTime> onDateSelected)
        {
            DatePickerFragment frag = new DatePickerFragment();
            frag._dateSelectedHandler = onDateSelected;
            _dateStart = dateStart;
            return frag;
        }

        public override Dialog OnCreateDialog(Bundle savedInstanceState)
        {
            //Date today = new Date();
            //Calendar c = Calendar.Instance;
            //c.Time = today; ;
            //c.Add(CalendarField.DayOfYear, 2);
            //long maxDate = c.Time.Time;

            DateTime currently = _dateStart;
            DatePickerDialog dialog = new DatePickerDialog(Activity, this, currently.Year, currently.Month - 1, currently.Day);
            //dialog.DatePicker.MaxDate = maxDate;
            return dialog;
        }

        public void OnDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
        {
            DateTime selectedDate = new DateTime(year, monthOfYear + 1, dayOfMonth);
            _dateSelectedHandler(selectedDate);
        }
    }
}