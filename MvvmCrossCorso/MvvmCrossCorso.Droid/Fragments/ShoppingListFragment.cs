﻿using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.Widget;
using Android.Support.V7.Widget;
using Android.Support.V7.Widget.Helper;
using Android.Views;
using MvvmCross.Droid.Support.V7.RecyclerView;
using MvvmCross.Droid.Views.Attributes;
using MvvmCross.Platform.WeakSubscription;
using MvvmCrossCorso.Core.ViewModels;
using MvvmCrossCorso.Droid.Helpers;
using System;

namespace MvvmCrossCorso.Droid.Fragments
{
    [MvxFragmentPresentation(typeof(MainViewModel), Resource.Id.content_frame)]
    [Register("com.companyname.MvvmCrossCorso.droid.fragments.ShoppingListFragment")]
    public class ShoppingListFragment : BaseFragment<ShoppingListViewModel>
    {
        protected override int FragmentId => Resource.Layout.fragment_shopping_list;
        SwipeRefreshLayout refresher;
        MvxNamedNotifyPropertyChangedEventSubscription<bool> hasErrorToken;
        View view;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            showHamburgerMenu = true;
            fragmentTitle = "Lista spesa";
            view = base.OnCreateView(inflater, container, savedInstanceState);
            var recyclerView = view.FindViewById<MvxRecyclerView>(Resource.Id.recycler_view);
            recyclerView.AddItemDecoration(new RecyclerViewItemDecoration(Activity));
            recyclerView.SetLayoutManager(new LinearLayoutManager(Activity));

            var callback = new SwipeShoppingItemTouchHelperCallback(ViewModel);
            var touchHelper = new ItemTouchHelper(callback);
            touchHelper.AttachToRecyclerView(recyclerView);

            refresher = view.FindViewById<SwipeRefreshLayout>(Resource.Id.refresher);
            refresher.Refresh += Refresher_Refresh;

            return view;
        }

        public override void OnResume()
        {
            base.OnResume();
            hasErrorToken = ViewModel.WeakSubscribe(() => ViewModel.HasError, (sender, eventArgs) =>
            {
                if (ViewModel.HasError)
                {
                    Snackbar.Make(view, ViewModel.ErrorDesc, Snackbar.LengthLong).Show();
                }
            });
        }

        public override void OnPause()
        {
            base.OnPause();
            hasErrorToken?.Dispose();
        }

        private async void Refresher_Refresh(object sender, EventArgs e)
        {
            await ViewModel.LoadShoppingItems();
            refresher.Refreshing = false;
        }
    }
}