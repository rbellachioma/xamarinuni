using MvvmCross.Platform.Plugins;

namespace MvvmCrossCorso.Droid.Bootstrap
{
    public class MessengerPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.Messenger.PluginLoader>
    {
    }
}