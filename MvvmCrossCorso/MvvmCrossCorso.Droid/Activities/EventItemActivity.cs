﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Widget;
using MvvmCross.Droid.Support.V7.AppCompat;
using MvvmCross.Droid.Support.V4;
using MvvmCrossCorso.Core.ViewModels;
using MvvmCrossCorso.Droid.Fragments;
using System;

namespace MvvmCrossCorso.Droid.Activities
{
    [Activity(
    Label = "Evento",
    Theme = "@style/MyTheme",
    LaunchMode = LaunchMode.SingleTop
    )]
    public class EventItemActivity : MvxAppCompatActivity<EventItemViewModel>
    {
        TextView eventDate;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_event_item);
            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar?.SetDisplayHomeAsUpEnabled(true);

            eventDate = (TextView)FindViewById(Resource.Id.lblEventDate);
            eventDate.Click += SearchDate_OnClick;
        }

        void SearchDate_OnClick(object sender, EventArgs eventArgs)
        {
            DatePickerFragment frag = DatePickerFragment.NewInstance(ViewModel.EventDate, delegate (DateTime time)
            {
                ViewModel.EventDate = time.Date;
            });
            frag.Show(SupportFragmentManager, DatePickerFragment.TAG);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    ViewModel.CancelCommand.Execute();
                    return true;
                case Resource.Id.action_save:
                    ViewModel.SaveCommand.Execute();
                    return true;
                default:
                    return base.OnOptionsItemSelected(item);
            }
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            base.OnCreateOptionsMenu(menu);
            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            toolbar.InflateMenu(Resource.Menu.event_item_menu);

            return true;
        }
    }
}