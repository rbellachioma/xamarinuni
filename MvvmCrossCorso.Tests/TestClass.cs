﻿using MvvmCrossCorso.Core.Contracts;
using MvvmCrossCorso.Core.Model;
using MvvmCrossCorso.Core.Services;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvvmCrossCorso.Tests
{
    [TestFixture]
    public class TestClass
    {
        IWebClientService service;
        [SetUp]
        public void SetUp()
        {
            service = new WebClientService();
        }

        [Test]
        public void CalculateGETPow_3_2_returns_9()
        {
            //Act
            double resp = service.getPow(3, 2).Result;

            //Assert
            Assert.AreEqual(9, resp);
            Assert.GreaterOrEqual(resp, 9);
        }

        [Test]
        public void CalculatePOSTPow_3_2_returns_9()
        {
            //Act
            RequestDTO req = new RequestDTO();
            req.numToCalculate = 3;
            req.pow = 2;
            req.username = "pippo";
            ResponseDTO resp = service.postPow(req).Result;

            //Assert
            Assert.AreEqual(9, resp.result);
            Assert.GreaterOrEqual(resp.result, 9);
        }
    }
}
